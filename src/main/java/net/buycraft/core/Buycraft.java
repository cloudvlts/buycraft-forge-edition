package net.buycraft.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import net.buycraft.commands.CommandBuy;
import net.buycraft.commands.CommandBuycraft;
import net.buycraft.commands.CommandEnableChat;
import net.buycraft.events.PlayerEvent;
import net.buycraft.events.PlayerTracker;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;

@Mod(modid="buycraft", name="Buycraft", version="0.0.1")
public class Buycraft {
    public static String secret_key;
    public static boolean autoupdate;
    public static boolean headsEnabled;
    public static boolean headsCurrency;
    public static String directPayGateway;
    public static boolean directPay;
    public static boolean commandChecker;
    public static boolean disableBuyCommand;
    public static boolean https;
    public static String buyCommand;

    Configuration config;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
        loadconfig();
    }

    @EventHandler
    public void startingServer(FMLServerStartingEvent event){
        MinecraftForge.EVENT_BUS.register(new PlayerEvent());
        MinecraftForge.EVENT_BUS.register(new PlayerTracker());
        event.registerServerCommand(new CommandEnableChat());
        event.registerServerCommand(new CommandBuy());
        event.registerServerCommand(new CommandBuycraft());
    }

    @EventHandler
    public void serverStopping(FMLServerStoppingEvent event){

    }

    public void loadconfig(){
        config = new Configuration(new File("config/buycraft.cfg"));
        config.load();
        secret_key = config.get("General", "secret_key", "").getString();
        autoupdate = config.get("General", "autoupdate", false).getBoolean(false);
        headsEnabled = config.get("General", "headsEnabled", false).getBoolean(false);
        headsCurrency = config.get("General", "headsCurrency", false).getBoolean(false);
        directPayGateway = config.get("General", "directPayGateway", "Paypal").getString();
        directPay = config.get("General", "directPay", false).getBoolean(false);
        commandChecker = config.get("General", "commandChecker", false).getBoolean(false);
        disableBuyCommand = config.get("General", "disableBuyCommand", false).getBoolean(false);
        https = config.get("General", "https", false).getBoolean(false);
        buyCommand = config.get("General", "buyCommand", "buy").getString();
        config.save();
    }
}
