package net.buycraft.core;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

import java.util.EnumSet;

public class TickHandler implements ITickHandler {
    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData) {
        //perform a check every other tick to see if anything new came up
    }

    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData) {
    }

    @Override
    public EnumSet<TickType> ticks() {
        return null;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
