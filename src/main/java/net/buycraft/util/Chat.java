package net.buycraft.util;

import net.buycraft.util.ChatColor;

public class Chat {
    private static final String header = ChatColor.white+ "|----------------------" + ChatColor.purpleBlue  + " BUYCRAFT " + ChatColor.white + "---------------------";
    private static final String footer = ChatColor.white + "|----------------------------------------------------";
    private static final String seperator = ChatColor.white + "| ";

    private Chat() {}

    public static String header() {
        return header;
    }

    public static String footer() {
        return footer;
    }

    public static String seperator() {
        return seperator;
    }
}