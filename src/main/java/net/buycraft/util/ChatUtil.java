package net.buycraft.util;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatMessageComponent;

public class ChatUtil {
    public static void sendMessage(ICommandSender isender, String message){
        isender.sendChatToPlayer(ChatMessageComponent.func_111077_e(message));
    }
}
