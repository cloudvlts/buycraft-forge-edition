package net.buycraft.events;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.ServerChatEvent;

public class PlayerEvent {
    @ForgeSubscribe
    public void onPlayerChat(ServerChatEvent event){
        if(event.player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG).getBoolean("bcmuted")){
            event.setCanceled(true);
            event.player.sendChatToPlayer(ChatMessageComponent.func_111066_d("Please use /ec to enable chat"));
            return;
        }
    }
}
