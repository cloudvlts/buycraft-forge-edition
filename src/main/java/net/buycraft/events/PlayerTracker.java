package net.buycraft.events;

import cpw.mods.fml.common.IPlayerTracker;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class PlayerTracker implements IPlayerTracker {
    @Override
    public void onPlayerLogin(EntityPlayer player) {
        if(player.username.equalsIgnoreCase("Buycraft")){
            ((EntityPlayerMP) player).playerNetServerHandler.kickPlayerFromServer("This user has been disabled due to security reasons.");
        }
        else
        {
            NBTTagCompound tag = player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
            if(tag.getBoolean("bcmuted")){
                tag.setBoolean("bcmuted", false);
                player.getEntityData().setCompoundTag(EntityPlayer.PERSISTED_NBT_TAG, tag);
            }
        }
    }

    @Override
    public void onPlayerLogout(EntityPlayer player) {
    }

    @Override
    public void onPlayerChangedDimension(EntityPlayer player) {
    }

    @Override
    public void onPlayerRespawn(EntityPlayer player) {
    }
}
