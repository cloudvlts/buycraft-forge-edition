package net.buycraft.commands;

import net.buycraft.core.Buycraft;
import net.buycraft.util.Chat;
import net.buycraft.util.ChatColor;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatMessageComponent;

public class CommandBuy extends CommandBase{
    private boolean authenticated = false;
    private int authenticatedCode = 1;
    @Override
    public String getCommandName() {
        return Buycraft.buyCommand;
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender isender, String[] astring) {
        if(isender instanceof EntityPlayerMP){
            EntityPlayerMP player = (EntityPlayerMP) isender;
            NBTTagCompound tag = player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
            if(!tag.getBoolean("bcmuted")){
                tag.setBoolean("bcmuted", true);
                player.getEntityData().setCompoundTag(EntityPlayer.PERSISTED_NBT_TAG, tag);
                player.sendChatToPlayer(ChatMessageComponent.func_111066_d("Your chat have been enabled"));
            }
        }
    }


    public Boolean isAuthenticated(ICommandSender commandSender) {
        if (!authenticated) {
            if (commandSender != null) {
                commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.header()));
                commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "Buycraft has failed to startup."));
                commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                if(authenticatedCode == 101)  {
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "This is because of an invalid secret key,"));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "please enter the Secret key into the settings.conf"));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "file, and reload your server."));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "You can find your secret key from the control panel."));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "If it did not resolve the issue, restart your server"));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "a couple of times."));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                } else {
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "Please execute the '/buycraft report' command and"));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "then send the generated report.txt file to"));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator() + ChatColor.red + "support@buycraft.net. We will be happy to help."));
                    commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.seperator()));
                }
                commandSender.sendChatToPlayer(ChatMessageComponent.func_111066_d(Chat.footer()));
            }

            return false;
        } else {
            return true;
        }
    }

    public void setAuthenticated(Boolean value) {
        authenticated = value;
    }

    public void setAuthenticatedCode(Integer value) {
        authenticatedCode = value;
    }

    public Integer getAuthenticatedCode() {
        return authenticatedCode;
    }

}
