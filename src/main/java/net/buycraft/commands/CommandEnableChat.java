package net.buycraft.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatMessageComponent;

public class CommandEnableChat extends CommandBase {
    @Override
    public String getCommandName() {
        return "ec";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender isender, String[] astring) {
        if(isender instanceof EntityPlayerMP){
            EntityPlayerMP player = (EntityPlayerMP) isender;
            NBTTagCompound tag = player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
            if(tag.getBoolean("bcmuted")){
                tag.setBoolean("bcmuted", false);
                player.getEntityData().setCompoundTag(EntityPlayer.PERSISTED_NBT_TAG, tag);
                player.sendChatToPlayer(ChatMessageComponent.func_111066_d("Your chat have been enabled"));
            }
            else
            {
                player.sendChatToPlayer(ChatMessageComponent.func_111066_d("Your chat is already enabled"));
            }
        }
    }
}
